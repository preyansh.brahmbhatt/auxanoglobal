package com.preyansh.auxanoglobalpractical

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

const val SHARED_PREF_NAME = "DEPARTMENT_SHARED_PREF"
const val DEPARTMENT_DATA = "department_data"

class SharedPrefUtils(val mActivity: Activity) {
    var sharedPreferences: SharedPreferences? = null
        get() {
            if (field == null)
                field = mActivity.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return field
        }

    var editor: SharedPreferences.Editor? = null
        get() {
            if (field == null) field = sharedPreferences!!.edit()
            return field
        }

    var departmentData: MutableList<DepartmentModel>? = null
        set(value) {
            editor?.putString(DEPARTMENT_DATA, Gson().toJson(value))
            editor?.apply()
        }
        get() {
            val json = sharedPreferences?.getString(DEPARTMENT_DATA, "")
            field = Gson().fromJson(json, object : TypeToken<MutableList<DepartmentModel>>(){}.type)
            return field
        }

}