package com.preyansh.auxanoglobalpractical

import android.util.Patterns
import androidx.appcompat.widget.AppCompatEditText
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.preyansh.auxanoglobalpractical.databinding.EmployeeListSingleItemBinding
import java.util.logging.Handler

class DepartmentVM : ViewModel() {
    var departmentList = mutableListOf<DepartmentModel>()
    val departmentObserver = MutableLiveData<MutableList<DepartmentModel>>()
    var departmentAdapter: DepartmentListAdapter? = null

    fun addNewDepartment(depName: String) {
        if (departmentList.filter { depName.equals(it.depName) }?.size == 0) {
            departmentList.add(
                DepartmentModel(
                    depName, mutableListOf()
                )
            )
            departmentObserver.value = departmentList
        }
    }

    fun getDepAdapter(): DepartmentListAdapter {
        return DepartmentListAdapter(departmentList).apply {
            getBinder { binding, item, position ->
                binding.recEmployeeList.adapter = getEmpAdapter(item)
                binding.setOnAddEployeeClick {
                    item.employeeList.add(EmployeeModel("",false, true))
                    notifyDataSetChanged()
                }
            }
        }
    }

    fun getEmpAdapter(item : DepartmentModel): EmployeeListAdapter {
        return EmployeeListAdapter(item.employeeList).apply {
            getBinder { empBinding: EmployeeListSingleItemBinding, empItem: EmployeeModel, empPosition: Int ->
                empBinding.txtEmpEmail.setOnFocusChangeListener { view, hasFocus ->
                    if (!hasFocus) {
                        if (view is AppCompatEditText) {
                            if (view.text.toString().length > 0 && view.text.isValidEmail()) {
                                empItem.isValid = true
                                android.os.Handler().post {
                                    notifyItemChanged(empPosition)
                                }
                            } else {
                                empItem.isValid = false
                                android.os.Handler().post {
                                    notifyItemChanged(empPosition)
                                }
                            }
                        }
                    }
                }
                empBinding.setOnRemoveClick {
                    this.mItems.remove(empItem)
                    if (this.mItems.size == 0) {
                        departmentList.remove(item)
                    }
                    this.notifyDataSetChanged()
                }
            }
        }
    }

}

fun CharSequence?.isValidEmail() = !isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()
