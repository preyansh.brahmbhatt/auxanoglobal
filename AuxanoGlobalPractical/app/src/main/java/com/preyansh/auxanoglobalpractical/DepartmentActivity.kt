package com.preyansh.auxanoglobalpractical

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.appcompat.widget.AppCompatEditText
import androidx.databinding.DataBindingUtil.*
import androidx.lifecycle.Observer
import com.preyansh.auxanoglobalpractical.databinding.ActivityDepartmentBinding
import com.preyansh.auxanoglobalpractical.databinding.EmployeeListSingleItemBinding
import kotlinx.android.synthetic.main.activity_department.*
import kotlinx.android.synthetic.main.department_list_single_item.*

class DepartmentActivity : AppCompatActivity() {

    val departmentVM : DepartmentVM by viewModels<DepartmentVM> {
        DepartmentVMFactory()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView<ActivityDepartmentBinding>(this, R.layout.activity_department)

        SharedPrefUtils(this).departmentData?.let {
            if (it.size > 0) {
                departmentVM.departmentList = it
                departmentVM.departmentObserver.value = it
            }
        }
        prepareDepartmentsSpinner(listOf(
            "Select Departments",
            "Android",
            "iOS",
            "Web"
        ))
        observeChanges()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.department_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_save -> {
                SharedPrefUtils(this).departmentData = departmentVM.departmentList
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun prepareDepartmentsSpinner(departments: List<String>) {
        spn_departments.adapter =
            ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                departments
            )

        spn_departments.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, p3: Long) {
                if (!adapterView?.getItemAtPosition(position).toString().equals("Select Departments"))
                    departmentVM.addNewDepartment(adapterView?.getItemAtPosition(position).toString())
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }
    }

    private fun observeChanges() {
        departmentVM.departmentObserver.observe(this, Observer { departmentList ->
            if (departmentVM.departmentAdapter == null) {
                departmentVM.departmentAdapter = departmentVM.getDepAdapter()
                rec_department_list.adapter = departmentVM.departmentAdapter
            } else {
                departmentVM.departmentAdapter?.notifyDataSetChanged()
            }
        })
    }
}