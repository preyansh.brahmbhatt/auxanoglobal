package com.preyansh.auxanoglobalpractical

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.preyansh.auxanoglobalpractical.databinding.DepartmentListSingleItemBinding


class DepartmentListAdapter(
    val mItems: MutableList<DepartmentModel>
) : RecyclerView.Adapter<DepartmentViewHolder>() {

    var binder: ((DepartmentListSingleItemBinding,DepartmentModel, Int)->Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DepartmentViewHolder {
        return DepartmentViewHolder(
            DataBindingUtil.inflate<DepartmentListSingleItemBinding>(
                LayoutInflater.from(parent.context),
                R.layout.department_list_single_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: DepartmentViewHolder, position: Int) {
        val department = mItems[position]
        holder.bind(department)
        binder?.invoke(holder.binding, department, position)
    }

    override fun getItemCount() = mItems.size

    fun getBinder(l : (DepartmentListSingleItemBinding,DepartmentModel, Int)->Unit) {
        binder = l
    }
}

class DepartmentViewHolder(
    val binding: DepartmentListSingleItemBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(department: DepartmentModel) {
        binding.department = department
    }
}