package com.preyansh.auxanoglobalpractical

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.preyansh.auxanoglobalpractical.databinding.DepartmentListSingleItemBinding
import com.preyansh.auxanoglobalpractical.databinding.EmployeeListSingleItemBinding


class EmployeeListAdapter(
    val mItems: MutableList<EmployeeModel>
) : RecyclerView.Adapter<EmployeeViewHolder>() {

    var binder: ((EmployeeListSingleItemBinding, EmployeeModel, Int)->Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeViewHolder {
        return EmployeeViewHolder(
            DataBindingUtil.inflate<EmployeeListSingleItemBinding>(
                LayoutInflater.from(parent.context),
                R.layout.employee_list_single_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) {
        val employee = mItems.get(position)
        holder.bind(employee)
        binder?.invoke(holder.binding, employee, position)
    }

    override fun getItemCount() = mItems.size

    fun getBinder(l : (EmployeeListSingleItemBinding, EmployeeModel, Int)->Unit) {
        binder = l
    }
}

class EmployeeViewHolder(
    val binding: EmployeeListSingleItemBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(employee: EmployeeModel) {
        binding.employee = employee
    }
}