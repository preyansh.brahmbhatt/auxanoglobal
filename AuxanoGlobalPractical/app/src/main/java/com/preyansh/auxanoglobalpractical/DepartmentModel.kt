package com.preyansh.auxanoglobalpractical

data class DepartmentModel(
    val depName: String,
    val employeeList : MutableList<EmployeeModel>
)

data class EmployeeModel(
    var email : String,
    var status : Boolean,
    var isValid: Boolean
)