package com.preyansh.auxanoglobalpractical

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class DepartmentVMFactory() : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DepartmentVM() as T
    }
}